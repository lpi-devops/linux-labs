<center style="background-color:lightgreen">
    <h1 style="color:black"> 
     Python-Labs
    </h1>
</center>

[Please Read License](#License)





# `LABs` 


## Before Labs

Here are some rules by which to abide while learning python or any programming in general:
- Keep Attention to details.
- Seek and spot differences.
- Ask if you don't posses the knowledge and if you're done, check - do not hurry to share.
- DO NOT COPY-PASTA.
- Be Persistent.
- Be Patient.
- Try to be as practical as possible - DO NOT LEARN SOMETHING AND LEAVE IT WITHOUT PRACTICE.


---
---

### Learn these __key words__ by heart and never use them as variables

table | is | for | you | to study
--- | --- | --- | --- | ---
False | True | None | is | not
from | finally | return | continue | try
def | lambda | nonlocal | while | and
del | and | or | global | with 
assert | as | if | elif | else
import | pass | break | raise | in
yield | for | except

--- 
---

## REPL

### Lab 1

- Open your Operating System (Suggested to be Linux for obvious reasons):
  - Validate that you have latest version of python installed (at least python 3.6)
  - Open your Python Running Environment
    - do basic math:  <!-- Should be guided at least at list and such -- >
      - 5 + 2
      - 4 * 3
      - 15 / 5
      - 2 ** 8
      - 5 % 2
      - 4 - 2 * 3
      - (4 - 2) * 3
    - do some string manipulation:
      - 'I' + 'Myself' + 'And' + 'Irene'
      - 'I' / 'kill' \ 'You' <!-- yes - it is an error-->
      - me * 3
    - do collection manipulation:
      - create empty list.
      - check the list length.
      - add new elementst to list:
        - 1,2,'3',3,'2','1',[2,3,4],(5,4,3),{'n':1, 'm':2}
      - check the length of the list again.
  <!--REPL needs to be explored as much as possible-->

## Input and Output

### Lab 1 

  - Open text editor of your choise and :
    - print the string `hello world`.
    - print your name.
    - print your last name.
    - print your DOB.
    - print your POB.
    - print that you admit that printing is fun.
    - print that you will print every statement from this day on.
    - print that python is easy language.
  - Try doing the same in REPL.


### Lab 2

<!-- Operators with printing -->
  - Open new file in text editor of your choise and:
    - print amount of cars your family has
    - print amount of burgers you have eaten last week. (if vegan/vegeterian then imagine vegan burger.)
    - print the substraction of  5 and 2.
    - print the sum of -7 and 5
    -  are the answers of last 2 ecercise the same ?
    -  is first bigger?
    -  is second bigger?

### Lab 3

 <!-- Variables and printing -->
 create python3 script that does a follows:
 - create variable `car` with value 100.
 - define value `space_in_car`  with float value 4
 - create value `drivers` with in 30
 - define variable `passangers` with value 90
 - calculate amount of cars not driven by subtracking `cars` from `drivers`
 - declare variable `cars_driven` which is a copy of `drivers`
 - calculate carpool capacity by multiplying `cars_driven` with `space_in_car`
 - calculate passanger per car by dividing amount of `passangers` with `cars_drivers`
 - print everything to standard output and describe what you are printing: e.g.

```py
print('amount of cars is', cars)
```
### Lab 4

 <!--additional printing-->
 create python3 script that does a follows:
 - create variable `my_name` and assign your name to it.
 - declare variable named `my_age` and save in it your age.
 - define a variable called `my_height` and save your height in it.
 - create variable `my_eyes` and assign your eye color to it.
 - create variable `my_teeth` and assign your teeth color to it.
 - create variable `my_hair` and assign your hail color to it.
 - **NOTE** some of your variable are suppose to be  ints and floats, so try to be as precise as possible.
 - print everything to output via standard output.

### Lab 5 

<!-- Strings and text | format join and split-->  
create python3 script that does a follows:
- create a variable named `types_of_people` and assign value of 10 to it.
- create a variable `binary` that holds the value 'binary'
- create a vriable `do_not` that holds the value 'don\'t'
- define a variable `sentance_a`  and `sentance_b` that holds next values respectively:
  - f"There are {types_of_people} types of people."
  - f"Those who know {binary} and those who {do_not}."
- create  avariable hilarious  with value `False`
- define variable named `evalution` that holds next line as a string:
  - "Isn't that joke so funny?! {}"
- Lets print it all out to standard output by combining things:
  - print `sentance_a` with `sentance_b`
  - print `evaluation` variable with `hilarious` in it.

### Lab 6

complex printing

### Lab 7 

printing way over your head

### Lab 8

input and print
additional ways to input and print

## Data Types

    ### Numeric
        #### Int
        #### Float
    ### String
    ### Boolean
    ### List
    ### Tuples
    ### Sets
    ### Dictionary
    ### Bytes, Bytearray, memoryview

    ### getting data type
    ### Casting data typesf


## Operators

## Slicing

## Control Flow

## Loops

## Functions

## Error Handling

## Working with files

## Python Modules

## Standard Library

## 3rd Party Libraries

### Requests

## OOP
























[back to top](#labs)

##### `License`:
#####  **_This Material is developed by Alex M. Schapelle and [VAIOLabs inc](https://vaiolabs.com) and is distributed under [GPLv3](../Linux-Labs/LICENSE)_** 
